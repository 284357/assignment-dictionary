extern string_equals
extern string_length

global find_value

;Принимает указатель на нуль-терминированную строку (rdi)
;						и указатель на начало словаря (rsi)
;Если слово найдено, вернёт адрес, иначе вернёт 0

find_value:
	test	rsi, rsi
	je		.not_found
	
	.loop:
		push	rdi
		push	rsi
		add		rsi, 8
		call	string_equals
		pop		rsi
		pop		rdi
		
		test	rax, rax
		jnz		.found
		mov		rsi, [rsi]
		test 	rsi, rsi
		je		.end
		jmp		.loop
	
	.found:
		call	string_length
		add		rsi, 9
		add 	rax, rsi
		ret
	
	.end:
		mov 	rax, rsi
		ret
	
	.not_found:
		xor 	rax, rax
		ret
