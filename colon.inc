%macro colon 2

%2:
	%ifdef l_value
		dq l_value
	%else 
		dq 0
	%endif
	
	db %1, 0
	
	%define l_value %2
%endmacro
